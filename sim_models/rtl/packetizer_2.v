//-----------------------------------------------------------------------------
// Title         : RTP/UDP/IP/Ethernet Verilog Code
// Project       : HDMI2Ethernet
//-----------------------------------------------------------------------------
// File          : packetizer.v
// Author        : Tariq B. Ahmad  <tariqbashir@gmail.com>
// Created       : 19.06.2014
// Last modified : 19.06.2014
//-----------------------------------------------------------------------------
// Description :  Creates RTP/UDP/IP/Ethernet packet from raw video stream 
//                received as 24 bit RGB values
//-----------------------------------------------------------------------------
// 
// 
// 
//------------------------------------------------------------------------------
// Modification history :
// 19.06.2014 : created
//-----------------------------------------------------------------------------

 
`timescale 1ns/1ps

module packetizer_2(
                    input             clk,
                    input             reset,
                    //  input [7:0]       packet_size_i,
	            input             start,
                    //input [23:0]      bytes,
                    //input             en,
                    //input             HSYNC,
                    //input             VSYNC,
                    //input [10:0]      HCOUNT,
                    //input [10:0]      VCOUNT,
                    //output reg        fifo_full,
                    output reg [3:0]  wr_flags_o,
	            output reg [31:0] wr_data_o,
	            output reg        wr_src_rdy_o,
	            input             wr_dst_rdy_i
                    //    output reg [1300*8-1:0] rtp_pkt //made as output for monitoring purpose.
                    // Remove when synthesizing the core
                    );
  

  

  //states

  localparam [20:0]
    IDLE       = 2**0,
    MAKE_RTP_PKT = 2**1,
    MAKE_IP_HDR1 = 2**2,
    CALC_IP_HDR1_CHKSUM    = 2**3,
    CALC_IP_HDR2_CHKSUM    = 2**4,
    CALC_IP_HDR3_CHKSUM    = 2**5,
    CALC_IP_HDR4_CHKSUM    = 2**6,
    CALC_IP_HDR5_CHKSUM    = 2**7,
    SOP                    = 2**8,
    TRANSMIT_HDR2           = 2**9,
    TRANSMIT_HDR3           = 2**10,
    TRANSMIT_HDR4           = 2**11,
    TRANSMIT_HDR5           = 2**12,
    TRANSMIT_HDR6           = 2**13, 
    TRANSMIT_HDR7           = 2**14,
    TRANSMIT_HDR8           = 2**15,
    TRANSMIT_HDR9           = 2**16,
    TRANSMIT_HDR10          = 2**17,
    TRANSMIT_HDR11          = 2**18,
    FORMAT_RTP             = 2**19,
    TRANSMIT_RTP           = 2**20,
    CLEANUP                = 2**21;
    
 

        
  reg [20:0]                         state, nxt_state;
  
  reg [15:0] pixel_cnt, pkt_cnt, line_cnt, frame_cnt, pkt_offset,
             line_no, seqn;
  reg [31:0] timest;
  reg [15:0] nxt_pixel_cnt, nxt_pkt_cnt, nxt_line_cnt, nxt_frame_cnt,
             nxt_pkt_offset, nxt_line_no, nxt_seqn;
  reg [31:0] nxt_timest;
  reg        nxt_fifo_full;

  reg [1281*8-1:0] rtp_bytes; 
  reg [1281*8-1:0] nxt_rtp_bytes;

  reg [1301*8-1:0] nxt_rtp_pkt;  //1281 bytes of data + 20 bytes of RTP header
  reg [1301*8-1:0] rtp_pkt;  //uncomment when synthesizing the core.
  reg [20*8-1:0]   rtp_hdr;  //20 byte RTP Header


  reg              nxt_wr_src_rdy_o;
  reg [3:0]        nxt_wr_flags_o;
  
  reg [1301*8-1:0] send_buffer,nxt_send_buffer;
  reg [15:0]       data_cnt, nxt_data_cnt;

  reg [31:0]       nxt_wr_data_o;
  reg [31:0]       data;
 
  reg              sof, nxt_sof;

  reg [24:0]       pixel_mem [1279:0];

  reg [10:0]       hcount, vcount, nxt_hcount, nxt_vcount;
  
  //temporary variables. DELETE THEM LATER
  reg              en;
  reg              VSYNC;  
  reg              fifo_full;           


  localparam IP_IDENTIFICATION = 16'haabb;
  localparam IP_FRAG = 13'd0;

  parameter SRC_PORT = 16'h1234;
  parameter DST_PORT = 16'h1234;
  parameter SRC_MAC = 48'h0018_3e01_53d5;
  parameter DST_MAC = 48'h782b_cb87_cc67;  //ubuntu mac
  parameter DST_IP = 32'hc0a8_0101;  //192.168.1.1
  parameter SRC_IP = 32'hc0a8_0102;  //192.168.1.2
  
  // Generate valid IP header checksums
  wire [15:0]      header_checksum;
  reg [31:0]       header_checksum_input;
  reg              header_checksum_reset, nxt_header_checksum_reset;
  
  ip_header_checksum ip_header_checksum (
                                         .clk(clk),
                                         .checksum(header_checksum),
                                         .header(header_checksum_input),
                                         .reset(header_checksum_reset)
                                         );
  
  
  // Calculate packet lengths
  wire [15:0] packet_length_udp, packet_length_ip, packet_length_rtp;
 
  assign packet_length_rtp = {2'b00,8'b0001_0100,6'b010101};  //1281+20 = 1301 bytes
  assign packet_length_udp = {2'b00,8'b0001_0100,6'b011101}; //udp payload +  8 byte hdr
  assign packet_length_ip  = {2'b00,8'b0001_0100,6'b110001}; // IP header adds 20 bytes to UDP packet

/* -----\/----- EXCLUDED -----\/-----
  assign packet_length_rtp = {2'b00,packet_size_i[7:0],6'b010101};  //1281+20 = 1301 bytes
  assign packet_length_udp = {2'b00,packet_size_i[7:0],6'b011101}; //udp payload +  8 byte hdr
  assign packet_length_ip  = {2'b00,packet_size_i[7:0],6'b110001}; // IP header adds 20 bytes to UDP packet
 -----/\----- EXCLUDED -----/\----- */
  
  
  reg [13:0]  packet_size_count = 0;
  
  //ip packet header (20 bytes)
  wire [31:0] header_1, header_2, header_3, header_4, header_5;
  
  assign header_1 = {16'h4500, packet_length_ip};  //{version,ihl}, ip packet length including ip header
  assign header_2 = {IP_IDENTIFICATION[15:0], 3'b000, IP_FRAG[12:0]}; // IP identification, fragment;
  assign header_3 = {16'h4011, 16'h0000}; // TTL, protocol=17 for udp, header checksum
  assign header_4 = SRC_IP;
  assign header_5 = DST_IP;
  
  
  
   initial // Read the memory contents from the file simple_rgb.txt in pixel_mem memory
     begin
       $readmemb("simple_rgb.txt", pixel_mem);
       $display("Pixel memory read successfully\n");
     end
  
  //Sequential logic
  always @(posedge clk or posedge reset)
    if(reset)
      begin
        state         <= #1 IDLE;
        pixel_cnt     <= #1 0;
        pkt_cnt       <= #1 0;
        line_cnt      <= #1 0;
        frame_cnt     <= #1 0;
        fifo_full     <= #1 0;
        pkt_offset    <= #1 0;
        line_no       <= #1 26;
        seqn          <= #1 0;
        timest        <= #1 32'h0;
        rtp_pkt       <= #1 0;
        rtp_bytes     <= #1 0;
        wr_data_o     <= #1 0;
        wr_flags_o    <= #1 0;
        wr_src_rdy_o  <= #1 0;
        data_cnt      <= #1 0;
        sof           <= #1 0;
        hcount        <= #1 0;
        vcount        <= #1 0;
header_checksum_reset <= #1 1;
      end // if (reset)
  
    else
      begin
        state         <= #1 nxt_state;
        pixel_cnt     <= #1 nxt_pixel_cnt;
        pkt_cnt       <= #1 nxt_pkt_cnt;
        line_cnt      <= #1 nxt_line_cnt;
        frame_cnt     <= #1 nxt_frame_cnt;
        fifo_full     <= #1 nxt_fifo_full;
        pkt_offset    <= #1 nxt_pkt_offset;
        line_no       <= #1 nxt_line_no;
        seqn          <= #1 nxt_seqn;
        timest        <= #1 nxt_timest;
        rtp_pkt       <= #1 nxt_rtp_pkt;
        rtp_bytes     <= #1 nxt_rtp_bytes;
        wr_data_o     <= #1 nxt_wr_data_o;
        wr_src_rdy_o  <= #1 nxt_wr_src_rdy_o;
        wr_flags_o    <= #1 nxt_wr_flags_o;
        data_cnt      <= #1 nxt_data_cnt;
        send_buffer   <= #1 nxt_send_buffer;
        sof           <= #1 nxt_sof;
        hcount        <= #1 nxt_hcount;
        vcount        <= #1 nxt_vcount;
header_checksum_reset <= #1 nxt_header_checksum_reset; //unassert reset
      end // else: !if(reset)
  





  
  //combinational logic

  always @*
    begin
      //assign defaults
      nxt_state        = state;
      nxt_pixel_cnt    = pixel_cnt;
      nxt_pkt_cnt      = pkt_cnt;
      nxt_line_cnt     = line_cnt;
      nxt_frame_cnt    = frame_cnt;
      nxt_fifo_full    = fifo_full;
      nxt_pkt_offset   = pkt_offset;
      nxt_line_no      = line_no;
      nxt_seqn         = seqn;
      nxt_timest       = timest;
      nxt_rtp_pkt      = rtp_pkt;
      nxt_rtp_bytes    = rtp_bytes;
      nxt_send_buffer  = send_buffer; 
      nxt_data_cnt       = data_cnt;
      nxt_wr_src_rdy_o   = wr_src_rdy_o;
      nxt_wr_flags_o     = wr_flags_o;
      nxt_wr_data_o      = wr_data_o;
      nxt_sof            = sof;
      nxt_hcount         = hcount;
      nxt_vcount         = vcount;
      nxt_header_checksum_reset = header_checksum_reset;
      
      case(state)
        IDLE:
          begin
            nxt_fifo_full = 1'b0;
           
            //Start of traffic (should see a HSYNC transition)
/* -----\/----- EXCLUDED -----\/-----
            if(HSYNC & ~VSYNC)  //or VCOUNT == 1
              nxt_sof = 1;
            
            if(en && sof)
              nxt_state = MAKE_PKT;
 -----/\----- EXCLUDED -----/\----- */
            en = pixel_mem[hcount][24];
            VSYNC = 0;  //temporary
            if(en)
              nxt_state = MAKE_RTP_PKT;
          end
        
        MAKE_RTP_PKT:
          begin
            if(en /*&& ~HSYNC*/ && (pkt_cnt < 2) && (pixel_cnt < 427) && (~fifo_full) )  //  427 RGB values
              begin
                nxt_rtp_bytes = {rtp_bytes,pixel_mem[hcount][23:0]};
                nxt_pixel_cnt = pixel_cnt + 1;
                nxt_hcount    = hcount + 1;
                if(pixel_cnt == 426)  //when received last pixel, backpressure
                  nxt_fifo_full = 1'b1;
              end
            else if (en && /*~HSYNC &&*/ (pkt_cnt < 2) && (pixel_cnt == 427) && (fifo_full) )   //packet formed
              begin
               
                nxt_pkt_cnt = pkt_cnt + 1     ;      // one packet captured successfully
                nxt_pkt_offset = pkt_offset + 1281;  //next packet's offset is 1281 from current
                nxt_seqn = seqn + 1;                 //next packet's seqn
                nxt_pixel_cnt = 0;                   //start the count from 0
                rtp_hdr = {8'h80,1'b0,7'd24,seqn,timest,32'h00000000,32'h0000_0501,line_no,pkt_offset};
                nxt_rtp_pkt = {rtp_hdr,rtp_bytes};
	        nxt_send_buffer = nxt_rtp_pkt;       //prepare send buffer
                nxt_header_checksum_reset = 0;  //unassert
                nxt_state = CALC_IP_HDR1_CHKSUM;
              end // if (pkt_cnt < 2 && pixel_cnt == 426)
            
            else if (en /*&& ~HSYNC*/ && pkt_cnt == 2 && pixel_cnt < 426 && (~fifo_full) )
              begin
                nxt_rtp_bytes = {rtp_bytes,pixel_mem[hcount][23:0]};
                nxt_pixel_cnt = pixel_cnt + 1;
                nxt_hcount    = hcount + 1;
                if(pixel_cnt == 425)             //when received last byte, backpressure
                  nxt_fifo_full = 1'b1;
              end // if (pkt_cnt == 2 && pixel_cnt < 426)

            else if (en && /*~HSYNC &&*/ pkt_cnt == 2 && pixel_cnt == 426 && (fifo_full) )
              begin
                nxt_line_cnt    = line_cnt + 1;    //move to next line
                nxt_line_no     = line_no + 1;     //line number is 26 + some number
                nxt_pkt_offset  = 0;               // reset offset to 0
                nxt_pkt_cnt     = 0;
                nxt_seqn        = seqn + 1;        //next packet's seqn
                nxt_pixel_cnt   = 0;               //start the count from 0
                rtp_hdr         = {8'h80,1'b0,7'd24,seqn,timest,32'h00000000,32'h0000_04fe,line_no,pkt_offset};
                nxt_rtp_pkt     = {rtp_hdr,rtp_bytes};
	        nxt_send_buffer = nxt_rtp_pkt;  //prepare send buffer
                nxt_header_checksum_reset = 0;  //unassert
                nxt_state = CALC_IP_HDR1_CHKSUM;
              end // if (en && pkt_cnt == 2 && pixel_cnt == 425)

            else if (VSYNC)
              begin
               
                nxt_pixel_cnt = 0;                   //start the count from 0
                nxt_hcount    = 0;
                //start the next frame
                if(line_cnt > 719)
                  begin
                    nxt_frame_cnt = nxt_frame_cnt + 1;
                    nxt_line_cnt = 0;
                    nxt_line_no = 26;
                    nxt_pkt_cnt = 0;
                    nxt_timest = nxt_timest + 1;
                  end
                //discard the current packet if the packet is not finished and start over
                else
                  begin
                    nxt_line_cnt = 0;
                    nxt_line_no = 26;
                    nxt_pkt_cnt = 0;
                  end
                nxt_state = IDLE;
              end // if (VSYNC)
            
          end // case: MAKE_PKT
        
        CALC_IP_HDR1_CHKSUM:
          begin
            header_checksum_input = header_1;
            nxt_state = CALC_IP_HDR2_CHKSUM;
          end

        CALC_IP_HDR2_CHKSUM:
            begin
              header_checksum_input = header_2;
              nxt_state = CALC_IP_HDR3_CHKSUM;
            end
        
        CALC_IP_HDR3_CHKSUM:
          begin
            header_checksum_input = header_3;
            nxt_state = CALC_IP_HDR4_CHKSUM;
          end
        
        CALC_IP_HDR4_CHKSUM:
          begin
            header_checksum_input = header_4;
            nxt_state = CALC_IP_HDR5_CHKSUM;
          end
        
        CALC_IP_HDR5_CHKSUM:
          begin
            header_checksum_input = header_5;
            nxt_state = SOP;
          end
        
        SOP:
          begin
            start_pkt();  //start of Ethernet frame
            transmit_header(DST_MAC[47:16]); //start with 32 bits of dst_mac address
            nxt_state = TRANSMIT_HDR2;
          end

        TRANSMIT_HDR2:
          begin
            transmit_header({DST_MAC[15:0], SRC_MAC[47:32]}); 
	    clear_mac_flags();
            nxt_state = TRANSMIT_HDR3;
          end
        
        TRANSMIT_HDR3:
          begin
            transmit_header(SRC_MAC[31:0]);
            nxt_state = TRANSMIT_HDR4;
          end

        TRANSMIT_HDR4:
          begin
            transmit_header({16'h0800, header_1[31:16]}); // First 8 bits: hwtype ethernet (4), protocol type ipv4 (1),  header length (1) (*4), dsc (2)
            nxt_state = TRANSMIT_HDR5;
          end

        TRANSMIT_HDR5:
          begin
            transmit_header({header_1[15:0], header_2[31:16]});
            nxt_state = TRANSMIT_HDR6;
          end

        TRANSMIT_HDR6:
          begin
            transmit_header({header_2[15:0], header_3[31:16]});
            nxt_state = TRANSMIT_HDR7;
          end

        TRANSMIT_HDR7:
          begin
            transmit_header({header_checksum, header_4[31:16]}); // Inject the calculated header checksum here
            nxt_state = TRANSMIT_HDR8;
          end

        TRANSMIT_HDR8:
          begin
            transmit_header({header_4[15:0], header_5[31:16]});
            nxt_state = TRANSMIT_HDR9;
          end

        TRANSMIT_HDR9:
          begin
            transmit_header({header_5[15:0], SRC_PORT});
            nxt_state = TRANSMIT_HDR10;
          end
        
        TRANSMIT_HDR10:
          begin
            transmit_header({DST_PORT, packet_length_udp});
            nxt_state = TRANSMIT_HDR11;
          end

        TRANSMIT_HDR11:
          begin
            transmit_header(32'h0000_4142); // UDP checksum (4), start of RTP header
            nxt_state = FORMAT_RTP;
          end
     
        FORMAT_RTP:
          begin
            if(data_cnt < 325)
              begin
                data = send_buffer[1301*8-1:1301*8-32]; //send 32 bytes
                nxt_data_cnt = nxt_data_cnt + 1;
                nxt_state = TRANSMIT_RTP;
              end
            else if(data_cnt == 325)  //send the last remainder byte
              begin
                data = send_buffer[1301*8-1:1301*8-32];
                nxt_data_cnt = nxt_data_cnt + 1;
                nxt_state =  TRANSMIT_RTP;
              end
            else
              begin
                nxt_state = CLEANUP;
/* -----\/----- EXCLUDED -----\/-----
                nxt_wr_flags_o = 4'b0010; //mark end of packet
                nxt_data_cnt  = 0;        //reset data count for the next packet
                nxt_wr_src_rdy_o = 0;
	        nxt_state = IDLE;   //accept new data
 -----/\----- EXCLUDED -----/\----- */
              end
          end // case: TRANSMIT_RTP_HDR_AND_PAYLOAD
        
        
         TRANSMIT_RTP:
          begin
           // transmit pkt
            if (wr_dst_rdy_i) 
              begin
	        nxt_wr_data_o  = data;    
                nxt_send_buffer = {send_buffer,32'h0}; //shift the buffer for next data word
                nxt_state = FORMAT_RTP;
              end
          end

        CLEANUP:
          begin
            if (wr_dst_rdy_i) // Wait until we're sure the last word has been received.
              begin
	        nxt_wr_src_rdy_o = 0;
	        nxt_wr_flags_o = 0;
                nxt_data_cnt  = 0;        //reset data count for the next packet
	        nxt_header_checksum_reset = 1;
	        nxt_state = IDLE ;
              end
          end
        
        
      endcase // case (state)
      
    end // always @ *
  
  
 
/* -----\/----- EXCLUDED -----\/-----
  task append_rtp_hdr;
    begin
      rtp_pkt = {16'h80_00,seqn,timest,32'h00000000,32'h0000_0500,line_no,pkt_offset,rtp_pkt};
    end
  endtask //
 -----/\----- EXCLUDED -----/\----- */

  task start_pkt;
    begin
      nxt_wr_src_rdy_o = 1;	
      nxt_wr_flags_o = 4'b0001; // Start of packet
    end

  endtask 
  

  task transmit_header;
    input [31:0] header;
    begin
      if (wr_dst_rdy_i)
       	nxt_wr_data_o = header;
    end
    
  endtask
  
  task clear_mac_flags;
    begin
      nxt_wr_flags_o = 4'b0000;
    end
  endtask
  
  
/* -----\/----- EXCLUDED -----\/-----
  task transmit_pkt;
    input [31:0] d;
    begin
      if (wr_dst_rdy_i) begin
	wr_data_o  = d; 
//	state <= state + 1'b1;
      end
    end
  endtask
 -----/\----- EXCLUDED -----/\----- */
  
  
  
endmodule // packetizer
