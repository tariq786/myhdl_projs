module tb_my_stroby;

reg clk;
reg rst;
wire [7:0] led;

initial begin
    $from_myhdl(
        clk,
        rst
    );
    $to_myhdl(
        led
    );
end

my_stroby dut(
    clk,
    rst,
    led
);

endmodule
