#testbench for fifo_up_if

from myhdl import *

import fifo_up_if

def test():
    clk = Signal(bool(0))
    rst = ResetSignal(0,active=1,async=True) #Xilinx spec didn't mention
                                             #specifically about reset
                                             #polarity
                                             
    up_interface = fifo_up_if.fifo_upstream_interface() #defining signals for the fifo
                                             #upstream interface
    def _test():
        #instantiating dut
        dut = fifo_up_if.fifo_up_if_ex(clk,
                            rst,
                            up_interface.din,
                            up_interface.wr_en,
                            up_interface.full
                            )
                        
        @always(delay(5))
        def clk_driver():
            clk.next = not clk
        
        
        @instance
        def tb_stim():
            rst.next = rst.active
            yield delay(20)
            rst.next = not rst.active
            yield delay(20)
            yield clk.posedge
            
            for i in range(50):
                up_interface.wr_en.next = True
                up_interface.din.next = i
                yield clk.posedge
                up_interface.wr_en.next = False
                yield clk.posedge    
	    
	    raise StopSimulation	
        
        return dut, clk_driver, tb_stim

    Simulation(traceSignals(_test)).run()
    toVerilog(fifo_up_if.fifo_up_if_ex,clk,rst,up_interface.din,up_interface.wr_en,up_interface.full)
                                                              
    toVHDL(fifo_up_if.fifo_up_if_ex,clk,rst,up_interface.din,up_interface.wr_en,up_interface.full)        
                                
if __name__ == "__main__":
    test()
                
        