This part of the project deals with modeling uvcRawFIFO downstream interface.
The main file that defines the downstream interface is fifo_down_if.py
The file that tests this interface is  test_fifo_down_if.py

To run MyHDL simulation, simply run the followin on linux command line
$python test_fifo_down_if.py

conversion directory is created as a result of running the above command
that contains Verilog code + testbench and VHDL code for the fifo_down_if.py

VCD dump is also created as a result of running the above command
