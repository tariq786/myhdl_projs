module tb_fifo_down_if_ex;

reg clk;
reg rst;
wire [23:0] dout;
reg rd_en;
wire empty;

initial begin
    $from_myhdl(
        clk,
        rst,
        rd_en
    );
    $to_myhdl(
        dout,
        empty
    );
end

fifo_down_if_ex dut(
    clk,
    rst,
    dout,
    rd_en,
    empty
);

endmodule
